call "%vsbin%\..\vcvarsall.bat"
set CommonCompilerFlags=-MTd -nologo -Gm- -GR- -EHsc- -Od -Oi -WX -W4 -wd4201 -wd4100 -wd4189 -wd4127 -wd4065 -wd4800 -FC -Z7
rem set CommonCompilerFlags=         -nologo -Gm- -GR- -EHsc- -Ox     -WX -W4 -wd4201 -wd4100 -wd4189 -wd4127 -wd4065 -FC -Z7 -FAs  -fp:except-
set Win32IncludePaths= /I .\src /I ..\lib\include  
set Win32LinkerPaths= -LIBPATH:..\lib\bin\
set Win32IncludeFlags= %Win32IncludePaths%
set Win32LinkerFlags= %Win32LinkerPaths% -incremental:no -opt:ref user32.lib gdi32.lib winmm.lib opengl32.lib GLU32.lib Shlwapi.lib XInput.lib assimp-vc120-mt.lib DevIL.lib ILU.lib ILUT.lib
IF NOT EXIST build mkdir build
pushd build
cl %CommonCompilerFlags%  /FeanimTestBed.exe ..\src\main.cpp  %win32IncludeFlags% /link %Win32LinkerFlags%
copy animTestBed.exe ..\root
popd
pause
