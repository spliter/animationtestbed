/*
 * quaternionf.h
 *
 *  Created on: 17-05-2011
 *      Author: Spliter
 */

#ifndef quaternionf_H_
#define quaternionf_H_

#include <math.h>
#include "vec3f.h"
#include "matrix4x4f.h"

struct lbAxisAngle
{
	vec3f axis;
	real32 angle;//rad
};

class quaternionf
{
public:
	union
	{
		struct
		{
			real32 x,y,z,w;
		};
		real32 v[4];
	};

	quaternionf()
	{
	}

	quaternionf(const quaternionf& other)
	{
		x = other.x;
		y = other.y;
		z = other.z;
		w = other.w;
	}

	quaternionf(real32 x, real32 y, real32 z, real32 w)
	{
		this->x = x;
		this->y = y;
		this->z = z;
		this->w = w;
	}

	quaternionf& set(real32 _x, real32 _y, real32 _z, real32 _w);
	quaternionf& setRot(real32 pitch, real32 yaw, real32 roll);
	quaternionf& setRotDeg(real32 pitch, real32 yaw, real32 roll);
	quaternionf& setDeg(real32 pitch, real32 yaw, real32 roll);
	quaternionf& setRot(real32 rad, const vec3f& vec);
	quaternionf& setRot(real32 rad, real32 nx, real32 ny, real32 nz);
	quaternionf& setRotDeg(real32 deg, const vec3f& vec);
	quaternionf& setRotDeg(real32 deg, real32 nx, real32 ny, real32 nz);

	quaternionf& normalize(real32 precision=1e-7);
	quaternionf normalized(real32 precision=1e-7);

	quaternionf& makeIdentity();

	//Warning! something is off with rotation order! It seems it's reverse in relation to matrices
	//TODO: fix this
	quaternionf& rotateByDeg(real32 deg, const vec3f& vec);
	quaternionf& rotateBy(real32 rad, const vec3f& vec);
	quaternionf& rotateBy(const quaternionf& quat);//same as operator*=
	quaternionf rotatedBy(const quaternionf& quat) const;//same as operator*

	quaternionf conjugate() const;

	bool operator==(const quaternionf& quat) const;
	bool operator!=(const quaternionf& quat) const;

	quaternionf& operator=(const quaternionf& quat);
	quaternionf& operator*=(const quaternionf& quat);
	quaternionf operator*(const quaternionf& quat) const;
	quaternionf& operator-=(const quaternionf& quat);
	quaternionf operator-() const;
	quaternionf operator-(const quaternionf& quat) const;
	quaternionf& operator+=(const quaternionf& quat);
	quaternionf operator+(const quaternionf& quat) const;

	quaternionf operator*(const real32& f) const;
	quaternionf operator-(const real32& f) const;
	quaternionf operator+(const real32& f) const;
	quaternionf operator/(const real32& f) const;

	vec3f operator*(const vec3f& v) const;

	matrix4x4f getRotationMatrix();

	lbAxisAngle toAxisAngle();

	static quaternionf getRotation(const matrix4x4f&);
	static quaternionf getRotation(vec3f vecFrom, vec3f vecTo);

	static quaternionf identity();
};


quaternionf _quaternionf();
quaternionf _quaternionf(const quaternionf& quat);
quaternionf _quaternionf(real32 _x, real32 _y, real32 _z, real32 _w);
quaternionf _quaternionf(real32 deg, vec3f axis);

quaternionf slerp(const quaternionf& lh, const quaternionf& rh, real32 ratio);
quaternionf trislerp(const quaternionf& a, const quaternionf& b, const quaternionf& c, real32 u, real32 v);
quaternionf slerpZero(const quaternionf from, real32 ratio);
quaternionf nlerp(const quaternionf& lh, const quaternionf& rh, real32 ratio);
quaternionf optimalLerp(const quaternionf& lh, const quaternionf& rh, real32 ratio);//chooses between slerp or lerp

quaternionf operator*(const real32& f,const quaternionf& quat);
quaternionf operator-(const real32& f,const quaternionf& quat);
quaternionf operator+(const real32& f,const quaternionf& quat);
quaternionf operator/(const real32& f,const quaternionf& quat);

quaternionf quaternionf::identity()
{
	return _quaternionf(0,0,0,1);
}

quaternionf _quaternionf()
{
	quaternionf result;
	result.x = result.y = result.z = 0;
	result.w = 1;
	return result;
}

quaternionf _quaternionf(const quaternionf& quat)
{
	quaternionf result;
	result.x = quat.x;
	result.y = quat.y;
	result.z = quat.z;
	result.w = quat.w;
	return result;
}

quaternionf _quaternionf(real32 _x, real32 _y, real32 _z, real32 _w)
{
	quaternionf result;
	result.x = _x;
	result.y = _y;
	result.z = _z;
	result.w = _w;
	return result;
}

quaternionf _quaternionf(real32 deg, vec3f axis)
{
	quaternionf result;
	result.setRotDeg(deg, axis);
	result.normalize();
	return result;
}

quaternionf& quaternionf::set(real32 _x, real32 _y, real32 _z, real32 _w)
{
	x = _x;
	y = _y;
	z = _z;
	w = _w;
	return *this;
}

quaternionf& quaternionf::setRot(real32 pitch, real32 yaw, real32 roll)
{
	double c1 = cos(pitch/2);
	double c2 = cos(yaw/2);
	double c3 = cos(roll/2);

	double s1 = sin(pitch/2);
	double s2 = sin(yaw/2);
	double s3 = sin(roll/2);

	double c1c2 = c1*c2;
	double s1s2 = s1*s2;

	w =(real32)(c1c2*c3 -  s1s2*s3);
	x =(real32)( s1*c2*c3 + c1*s2*s3);
	y =(real32)( c1*s2*c3 - s1*c2*s3);
	z =(real32)( c1c2*s3 +  s1s2*c3);
	return *this;
}

quaternionf& quaternionf::setRotDeg(real32 pitch, real32 yaw, real32 roll)
{
	setRot(degToRad(pitch), degToRad(yaw), degToRad(roll));
	return *this;
}

quaternionf& quaternionf::setRot(real32 rad, const vec3f& vec)
{
	setRot(rad, vec.x, vec.y, vec.z);
	return *this;
}

quaternionf& quaternionf::setRot(real32 rad, real32 nx, real32 ny, real32 nz)
{
	real32 ls = nx*nx+ny*ny+nz*nz;
	real32 epsilon =(real32) 1e-7;
	if(ls<epsilon)
	{
		set(0.0, 0.0, 0.0, 1.0);
		return *this;
	}
	real32 invNorm = sqrt(ls);
	real32 cosHalf = (real32)cos(0.5*rad);
	real32 sinHalf = (real32)sin(0.5*rad);

	x = nx*sinHalf*invNorm;
	y = ny*sinHalf*invNorm;
	z = nz*sinHalf*invNorm;
	w = cosHalf;
	return normalize();
}

quaternionf& quaternionf::setRotDeg(real32 deg, const vec3f& vec)
{
	return setRot((real32)(deg*DEG_TO_RAD_CONST), vec.x, vec.y, vec.z);
}

quaternionf& quaternionf::setRotDeg(real32 deg, real32 nx, real32 ny, real32 nz)
{
	return setRot((real32)(deg*DEG_TO_RAD_CONST), nx, ny, nz);
}

quaternionf& quaternionf::normalize(real32 precision)
{
	real32 dsqr = x*x+y*y+z*z+w*w;

	if(fabs(dsqr-1.0)>precision)
	{
		real32 d1 =(real32)( 1.0f/sqrt(dsqr));
		x *= d1;
		y *= d1;
		z *= d1;
		w *= d1;
	}
	return *this;
}

quaternionf quaternionf::normalized(real32 precision)
{
	quaternionf ret(*this);
	ret.normalize(precision);
	return ret;
}

quaternionf& quaternionf::makeIdentity()
{
	return set(0.0, 0.0, 0.0, 1.0);
}

quaternionf& quaternionf::rotateByDeg(real32 deg, const vec3f& vec)
{
	quaternionf aux;
	aux.setRotDeg(deg, vec);
	return rotateBy(aux);
}

quaternionf& quaternionf::rotateBy(real32 rad, const vec3f& vec)
{
	quaternionf aux;
	aux.setRot(rad, vec);
	return rotateBy(aux);
}

quaternionf& quaternionf::rotateBy(const quaternionf& rhs)
{
//	set(rhs.w*x+rhs.x*w+rhs.y*z-rhs.z*y, rhs.w*y-rhs.x*z+rhs.y*w+rhs.z*x, rhs.w*z+rhs.x*y-rhs.y*x+rhs.z*w, rhs.w*w-rhs.x*x-rhs.y*y-rhs.z*z);
	set(w*rhs.x+x*rhs.w+y*rhs.z-z*rhs.y,
		w*rhs.y-x*rhs.z+y*rhs.w+z*rhs.x,
		w*rhs.z+x*rhs.y-y*rhs.x+z*rhs.w,
		w*rhs.w-x*rhs.x-y*rhs.y-z*rhs.z
		);
	return *this;
}

quaternionf quaternionf::rotatedBy(const quaternionf& rhs) const
{
	return _quaternionf(w*rhs.x+x*rhs.w+y*rhs.z-z*rhs.y,
		w*rhs.y-x*rhs.z+y*rhs.w+z*rhs.x,
		w*rhs.z+x*rhs.y-y*rhs.x+z*rhs.w,
		w*rhs.w-x*rhs.x-y*rhs.y-z*rhs.z);
}

quaternionf quaternionf::conjugate() const
{
	return _quaternionf(-x, -y, -z, w);

}

bool quaternionf::operator==(const quaternionf& quat) const
{
	return x==quat.x&&y==quat.y&&z==quat.z&&w==quat.w;
}

bool quaternionf::operator!=(const quaternionf& quat) const
{
	return x!=quat.x||y!=quat.y||z!=quat.z||w!=quat.w;
}

quaternionf& quaternionf::operator=(const quaternionf& quat)
{
	x = quat.x;
	y = quat.y;
	z = quat.z;
	w = quat.w;
	return *this;
}

quaternionf& quaternionf::operator*=(const quaternionf& quat)
{
	return rotateBy(quat);
}

quaternionf quaternionf::operator*(const quaternionf& quat) const
{
	return rotatedBy(quat);
}

quaternionf& quaternionf::operator-=(const quaternionf& quat)
{
	x -= quat.x;
	y -= quat.y;
	z -= quat.z;
	w -= quat.w;
	return *this;
}

quaternionf quaternionf::operator-() const
{
	return _quaternionf(-x, -y, -z, -w);
}

quaternionf quaternionf::operator-(const quaternionf& quat) const
{
	return _quaternionf(x-quat.x, y-quat.y, z-quat.z, w-quat.w);
}

quaternionf& quaternionf::operator+=(const quaternionf& quat)
{
	x += quat.x;
	y += quat.y;
	z += quat.z;
	w += quat.w;
	return *this;
}

quaternionf quaternionf::operator+(const quaternionf& quat) const
{
	return _quaternionf(x+quat.x, y+quat.y, z+quat.z, w+quat.w);
}

quaternionf quaternionf::operator*(const real32& f) const
{
	return _quaternionf(x*f, y*f, z*f, w*f);
}

quaternionf quaternionf::operator/(const real32& f) const
{
	return _quaternionf(x/f, y/f, z/f, w/f);
}

vec3f quaternionf::operator*(const vec3f& v) const
{
	//Algorithm courtesy of Fabian Giensen, found on http://molecularmusings.wordpress.com/2013/05/24/a-faster-quaternion-vector-multiplication/
	vec3f t = 2*_vec3f(x,y,z).cross(v);
	vec3f nt = v+t*w+_vec3f(x,y,z).cross(t);
	return nt;
}

matrix4x4f quaternionf::getRotationMatrix()
{
	//Note: Code copied from  gpWiki
	//http://www.gpwiki.org/index.php/OpenGL:Tutorials:Using_Quaternions_to_represent_rotation

	real32 x2 = x*x;
	real32 y2 = y*y;
	real32 z2 = z*z;
	real32 xy = x*y;
	real32 xz = x*z;
	real32 yz = y*z;
	real32 wx = w*-x;
	real32 wy = w*-y;
	real32 wz = w*-z;

	// This calculation would be a lot more complicated for non-unit length quaternions
	// Note: The constructor of Matrix4 expects the Matrix in column-major format like expected by
	//   OpenGL
	return matrix4x4f(1.0f-2.0f*(y2+z2), 2.0f*(xy-wz), 2.0f*(xz+wy), 0.0f, 2.0f*(xy+wz), 1.0f-2.0f*(x2+z2), 2.0f*(yz-wx), 0.0f, 2.0f*(xz-wy), 2.0f*(yz+wx), 1.0f-2.0f*(x2+y2), 0.0f, 0.0f, 0.0f,
			0.0f, 1.0f);
}

lbAxisAngle quaternionf::toAxisAngle()
{
	lbAxisAngle aangle;
	aangle.angle = 2*acosf(w);
	real32 s =(real32) sqrt(1.0-w*w);
	if(s<0.00001)
	{
		aangle.axis.set(x/s, y/s, z/s);
	}
	else
	{
		aangle.axis.set(x, y, z);
	}
	return aangle;
}

quaternionf quaternionf::getRotation(const matrix4x4f &mat)
{
	quaternionf result;
	result = quaternionf::getRotation(vec3f{mat.m20,mat.m21,mat.m22},vec3f{mat.m10,mat.m11,mat.m12});
	return result;
}

quaternionf quaternionf::getRotation(vec3f vecFrom, vec3f vecTo)
{
	vecFrom.normalize();
	vecTo.normalize();

	real32 cs = vecFrom.dot(vecTo);
	vec3f v = vecFrom.crossed(vecTo);

	real32 qw = 1+cs;
	if(cs<-1+0.00001)
		return _quaternionf(0.0, 1.0, 0.0, 0.0);

	quaternionf rot;
	rot.set(v.x, v.y, v.z, qw);
	rot.normalize();
	return rot;
}

quaternionf slerp(const quaternionf& from, const quaternionf& to, real32 ratio)
{
	real32 dot = from.x*to.x+from.y*to.y+from.z*to.z+from.w*to.w;

	real32 precision = 0.9995f;

	quaternionf auxTo(to);

	if(dot<0)
	{
		dot = -dot;
		auxTo = -to;
	}

	if(dot>precision)
	{
		//if angles are too close then just linearly interpolate
		quaternionf result = from+(auxTo-from)*ratio;
		result.normalize();
		return result;
	}

	real32 omega = acos(dot);
	real32 somega = sin(omega);

	if(somega==0.0)
		somega = 0.0001f;
	real32 fromRatio = (real32)(sin((1.0-ratio)*omega)/somega);
	real32 toRatio =(real32)( sin(ratio*omega)/somega);

	return ((from*fromRatio)+(auxTo*toRatio));
}

quaternionf trislerp(const quaternionf& a, const quaternionf& b, const quaternionf& c, real32 u, real32 v)
{
	real32 abRatio = 0.0f;
	real32 cRatio = (1-(v+u));
	if(u+v>0.000001f)
	{
		abRatio = u/(u+v);
	}
	return slerp(slerp(b,a,abRatio),c,cRatio);
}

quaternionf slerpZero(const quaternionf from, real32 ratio)
{
	real32 dot = from.w;

	real32 precision = 0.9995f;

	quaternionf auxTo;
	auxTo.set(0.0, 0.0, 0.0, 1.0);

	if(dot<0)
	{
		dot = -dot;
		auxTo.set(0.0, 0.0, 0.0, -1.0);
	}

	if(dot>precision)
	{
		//if angles are too close then just linearly interpolate
		quaternionf result = from+(auxTo-from)*ratio;
		result.normalize();
		return result;
	}

	real32 omega = acos(dot);
	real32 somega = sin(omega);

	if(somega==0.0)
	{
		somega = 0.0001f;
	}
	real32 fromRatio = (real32) sin((1.0-ratio)*omega)/somega;
	real32 toRatio = sin(ratio*omega)/somega;

	return ((from*fromRatio)+(auxTo*toRatio));
}

quaternionf operator*(const real32& f, const quaternionf& quat)
{
	return _quaternionf(quat.x*f, quat.y*f, quat.z*f, quat.w*f);
}

quaternionf operator-(const real32& f, const quaternionf& quat)
{
	return _quaternionf(f-quat.x, f-quat.y, f-quat.z, f-quat.w);
}

quaternionf operator+(const real32& f, const quaternionf& quat)
{
	return _quaternionf(f+quat.x, f+quat.y, f+quat.z, f+quat.w);
}

quaternionf operator/(const real32& f, const quaternionf& quat)
{
	return _quaternionf(f/quat.x, f/quat.y, f/quat.z, f/quat.w);
}
#endif /* quaternionf_H_ */
