/*
 * vecMathUtils.h
 *
 *  Created on: 4 Apr 2017
 *      Author: MikoKuta
 */
#pragma once

#include "vec2f.h"
#include "vec3f.h"
#include "../shared/types.h"

vec2f barycentricToCartesian(const vec3f &uvw, const vec2f &a, const vec2f &b, const vec2f &c);
vec3f cartesianToBarycentric(vec2f p, const vec2f &a, const vec2f &b, const vec2f &c);
bool32 closestPointsOnLine(const vec3f &lineStartA, const vec3f &lineDirA, const vec3f &lineStartB, const vec3f &lineDirB, vec3f& outClosestPointOnA, vec3f& outClosestPointOnB, real32 &outClosestTimeOnA, real32 &outClosestTimeOnB);
