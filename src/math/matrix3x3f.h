/*
 * matrix3x3f.h
 *
 *  Created on: 13-04-2011
 *      Author: Spliter
 */

#pragma once
#include <float.h>
#include "mathUtils.h"
#include "vec2f.h"

/* Note: Column major matrices
 * pre-multiply for the desired result
 * ie: to rotate a translated matrix, you'd have to do:
 * Result=rotation*translation;
 */
struct matrix3x3f
{
public:
	union
	{
		real32 me[9];
		struct
		{
			real32 m00;
			real32 m10;
			real32 m20;

			real32 m01;
			real32 m11;
			real32 m21;

			real32 m02;
			real32 m12;
			real32 m22;
		};
	};

	matrix3x3f(){};
	matrix3x3f(real32 val);
	matrix3x3f(	real32 _a,real32 _b,real32 _c,
				real32 _d,real32 _e,real32 _f,
				real32 _g,real32 _h,real32 _i);
	matrix3x3f(const matrix3x3f& mb);

	void set(real32 val);
	void set(	real32 _a,real32 _b,real32 _c,
				real32 _d,real32 _e,real32 _f,
				real32 _g,real32 _h,real32 _i);
	void set(const matrix3x3f& mb);

	matrix3x3f& makeIdentity();
	matrix3x3f& makeRotation(real32 rad);
	matrix3x3f& makeRotationDeg(real32 deg);
	matrix3x3f& makeScale(real32 xscale,real32 yscale);
	matrix3x3f& makeTranslation(real32 xtranslate,real32 ytranslate);

	matrix3x3f& transpose();
	matrix3x3f transposed() const;

	matrix3x3f& inverse();
	matrix3x3f inversed() const;

	matrix3x3f& rotate(real32 rad);
	matrix3x3f rotated(real32 rad) const;
	matrix3x3f& rotateDeg(real32 deg);
	matrix3x3f rotatedDeg(real32 deg) const;

	matrix3x3f& scale(real32 _scale);
	matrix3x3f scaled(real32 _scale) const;
	matrix3x3f& scale(real32 xscale,real32 yscale);
	matrix3x3f scaled(real32 xscale,real32 _yscale) const;

	matrix3x3f& translate(real32 x,real32 y);
	matrix3x3f translated(real32 x,real32 y) const;


	matrix3x3f& transform(const matrix3x3f& b);
	matrix3x3f transformed(const matrix3x3f& b) const;

	matrix3x3f& operator=(const matrix3x3f& b);
	matrix3x3f& operator*=(const matrix3x3f& b);
	matrix3x3f& operator/=(const matrix3x3f& b);
	matrix3x3f& operator+=(const matrix3x3f& b);
	matrix3x3f& operator-=(const matrix3x3f& b);

	matrix3x3f operator*(const matrix3x3f& b) const;
	matrix3x3f operator/(const matrix3x3f& b) const;
	matrix3x3f operator+(const matrix3x3f& b) const;
	matrix3x3f operator-(const matrix3x3f& b) const;

	matrix3x3f& operator=(real32 val);
	matrix3x3f& operator*=(real32 val);
	matrix3x3f& operator/=(real32 val);

	matrix3x3f operator*(real32 val) const;
	matrix3x3f operator/(real32 val) const;

	vec2f operator*(const vec2f& v) const;
};


matrix3x3f::matrix3x3f(real32 val)
{
	m00=val;m01=val;m02=val;
	m10=val;m11=val;m12=val;
	m20=val;m21=val;m22=val;
}

matrix3x3f::matrix3x3f(	real32 _a,real32 _b,real32 _c,
			real32 _d,real32 _e,real32 _f,
			real32 _g,real32 _h,real32 _i)
{
	m00=_a;m01=_d;m02=_g;
	m10=_b;m11=_e;m12=_h;
	m20=_c;m21=_f;m22=_i;
}

matrix3x3f::matrix3x3f(const matrix3x3f& mb)
{
	m00=mb.m00;m01=mb.m01;m02=mb.m02;
	m10=mb.m10;m11=mb.m11;m12=mb.m12;
	m20=mb.m20;m21=mb.m21;m22=mb.m22;
}

void matrix3x3f::set(real32 val)
{
	m00=val;m01=val;m02=val;
	m10=val;m11=val;m12=val;
	m20=val;m21=val;m22=val;
}

void matrix3x3f::set(	real32 _a,real32 _b,real32 _c,
			real32 _d,real32 _e,real32 _f,
			real32 _g,real32 _h,real32 _i)
{
	m00=_a;m01=_d;m02=_g;
	m10=_b;m11=_e;m12=_h;
	m20=_c;m21=_f;m22=_i;
}

void matrix3x3f::set(const matrix3x3f& mb)
{
	m00=mb.m00;m01=mb.m01;m02=mb.m02;
	m10=mb.m10;m11=mb.m11;m12=mb.m12;
	m20=mb.m20;m21=mb.m21;m22=mb.m22;
}

matrix3x3f& matrix3x3f::makeIdentity()
{
	me[0]=1.0;me[3]=0.0;me[6]=0.0;
	me[1]=0.0;me[4]=1.0;me[7]=0.0;
	me[2]=0.0;me[5]=0.0;me[8]=1.0;
	return *this;
}

matrix3x3f& matrix3x3f::makeRotation(real32 rad)
{
	makeIdentity();
	real32 m20=cos(rad);
	real32 s=sin(rad);
	me[0]=m20;me[3]=-s;
	me[1]=s;me[4]=m20;
	return *this;
}

matrix3x3f& matrix3x3f::makeRotationDeg(real32 deg)
{
	return makeRotation((real32)(deg*DEG_TO_RAD_CONST));
}

matrix3x3f& matrix3x3f::makeScale(real32 xscale,real32 yscale)
{
	makeIdentity();
	me[0]=xscale;
	me[4]=yscale;
	return *this;
}

matrix3x3f& matrix3x3f::makeTranslation(real32 xtranslate,real32 ytranslate)
{
	makeIdentity();
	me[6]=xtranslate;
	me[7]=ytranslate;
	return *this;
}

matrix3x3f& matrix3x3f::transpose()
{
	m10+=m01;m01=m10-m01;m10-=m01;
	m20+=m02;m02=m20-m02;m20-=m02;
	m21+=m12;m12=m21-m12;m21-=m12;
	return *this;
}

matrix3x3f matrix3x3f::transposed() const
{
	matrix3x3f result;
	result.set(m00,m01,m02,m10,m11,m12,m20,m21,m22);
	return result;
}

matrix3x3f& matrix3x3f::inverse()
{
	matrix3x3f inv = inversed();
	set(inv);
	return *this;
}

matrix3x3f matrix3x3f::inversed() const
{
	// computes the inverse of m00 matrix m
	real64 det =
			m00*  (m11 * m22 - m21 * m12) -
			m01 * (m10 * m22 - m12 * m20) +
			m02 * (m10 * m21 - m11 * m20);

	matrix3x3f minv; // inverse of matrix m
	if(fabs(det)>FLT_EPSILON)
	{
		real64 invdet = 1.0f / det;
		minv.m00 = (real32)((m11 * m22 - m21 * m12) * invdet);
		minv.m01 = (real32)((m02 * m21 - m01 * m22) * invdet);
		minv.m02 = (real32)((m01 * m12 - m02 * m11) * invdet);
		minv.m10 = (real32)((m12 * m20 - m10 * m22) * invdet);
		minv.m11 = (real32)((m00 * m22 - m02 * m20) * invdet);
		minv.m12 = (real32)((m10 * m02 - m00 * m12) * invdet);
		minv.m20 = (real32)((m10 * m21 - m20 * m11) * invdet);
		minv.m21 = (real32)((m20 * m01 - m00 * m21) * invdet);
		minv.m22 = (real32)((m00 * m11 - m10 * m01) * invdet);
	}
	else
	{
		minv.makeIdentity();
	}
	return minv;
}

matrix3x3f& matrix3x3f::rotate(real32 rad)
{
	matrix3x3f m;
	m.makeRotation(rad);
	return transform(m);
}

matrix3x3f matrix3x3f::rotated(real32 rad) const
{
	matrix3x3f m;
	m.makeRotation(rad);
	return transformed(m);
}

matrix3x3f& matrix3x3f::rotateDeg(real32 deg)
{
	matrix3x3f m;
	m.makeRotationDeg(deg);
	return transform(m);
}

matrix3x3f matrix3x3f::rotatedDeg(real32 deg) const
{
	matrix3x3f m;
	m.makeRotationDeg(deg);
	return transformed(m);
}

matrix3x3f& matrix3x3f::scale(real32 scale)
{
	matrix3x3f m;
	m.makeScale(scale,scale);
	return transform(m);
}

matrix3x3f matrix3x3f::scaled(real32 scale) const
{
	matrix3x3f m;
	m.makeScale(scale,scale);
	return transformed(m);
}

matrix3x3f& matrix3x3f::scale(real32 xscale,real32 yscale)
{
	matrix3x3f m;
	m.makeScale(xscale,yscale);
	return transform(m);
}

matrix3x3f matrix3x3f::scaled(real32 _xscale,real32 _yscale) const
{
	matrix3x3f m;
	m.makeScale(_xscale,_yscale);
	return transformed(m);
}

matrix3x3f& matrix3x3f::translate(real32 x,real32 y)
{
	matrix3x3f m;
	m.makeTranslation(x,y);
	return transform(m);
}

matrix3x3f matrix3x3f::translated(real32 x,real32 y) const
{
	matrix3x3f m;
	m.makeTranslation(x,y);
	return transformed(m);
}

matrix3x3f& matrix3x3f::transform(const matrix3x3f& mb)
{
	matrix3x3f tmpm;
	tmpm.set(			m00*mb.m00+m01*mb.m10+m02*mb.m20,
						m10*mb.m00+m11*mb.m10+m12*mb.m20,
						m20*mb.m00+m21*mb.m10+m22*mb.m20,

						m00*mb.m01+m01*mb.m11+m02*mb.m21,
						m10*mb.m01+m11*mb.m11+m12*mb.m21,
						m20*mb.m01+m21*mb.m11+m22*mb.m21,

						m00*mb.m02+m01*mb.m12+m02*mb.m22,
						m10*mb.m02+m11*mb.m12+m12*mb.m22,
						m20*mb.m02+m21*mb.m12+m22*mb.m22);
	set(tmpm);
	return *this;
}

matrix3x3f matrix3x3f::transformed(const matrix3x3f& mb) const
{
	matrix3x3f result;
	result.set(			m00*mb.m00+m01*mb.m10+m02*mb.m20,
						m10*mb.m00+m11*mb.m10+m12*mb.m20,
						m20*mb.m00+m21*mb.m10+m22*mb.m20,

						m00*mb.m01+m01*mb.m11+m02*mb.m21,
						m10*mb.m01+m11*mb.m11+m12*mb.m21,
						m20*mb.m01+m21*mb.m11+m22*mb.m21,

						m00*mb.m02+m01*mb.m12+m02*mb.m22,
						m10*mb.m02+m11*mb.m12+m12*mb.m22,
						m20*mb.m02+m21*mb.m12+m22*mb.m22);
	return result;
}

matrix3x3f& matrix3x3f::operator=(const matrix3x3f& mb)
{
	m00=mb.m00;m01=mb.m01;m02=mb.m02;
	m10=mb.m10;m11=mb.m11;m12=mb.m12;
	m20=mb.m20;m21=mb.m21;m22=mb.m22;
	return *this;
}

matrix3x3f& matrix3x3f::operator*=(const matrix3x3f& mb)
{
	return transform(mb);
}

matrix3x3f& matrix3x3f::operator/=(const matrix3x3f& mb)
{
	m00/=mb.m00;m01/=mb.m01;m02/=mb.m02;
	m10/=mb.m10;m11/=mb.m11;m12/=mb.m12;
	m20/=mb.m20;m21/=mb.m21;m22/=mb.m22;
	return *this;
}

matrix3x3f& matrix3x3f::operator+=(const matrix3x3f& mb)
{
	m00+=mb.m00;m01+=mb.m01;m02+=mb.m02;
	m10+=mb.m10;m11+=mb.m11;m12+=mb.m12;
	m20+=mb.m20;m21+=mb.m21;m22+=mb.m22;
	return *this;
}

matrix3x3f& matrix3x3f::operator-=(const matrix3x3f& mb)
{
	m00-=mb.m00;m01-=mb.m01;m02-=mb.m02;
	m10-=mb.m10;m11-=mb.m11;m12-=mb.m12;
	m20-=mb.m20;m21-=mb.m21;m22-=mb.m22;
	return *this;
}

matrix3x3f matrix3x3f::operator*(const matrix3x3f& mb) const
{
	return transformed(mb);
}

matrix3x3f matrix3x3f::operator/(const matrix3x3f& mb) const
{
	matrix3x3f result;
	result.set(m00/mb.m00,m10/mb.m10,m20/mb.m20,
					m01/mb.m01,m11/mb.m11,m21/mb.m21,
					m02/mb.m02,m12/mb.m12,m22/mb.m22);
	return result;
}

matrix3x3f matrix3x3f::operator+(const matrix3x3f& mb) const
{
	matrix3x3f result;
	result.set(m00+mb.m00,m10+mb.m10,m20+mb.m20,
					m01+mb.m01,m11+mb.m11,m21+mb.m21,
					m02+mb.m02,m12+mb.m12,m22+mb.m22);
	return result;
}

matrix3x3f matrix3x3f::operator-(const matrix3x3f& mb) const
{
	matrix3x3f result;
	result.set(m00-mb.m00,m10-mb.m10,m20-mb.m20,
					m01-mb.m01,m11-mb.m11,m21-mb.m21,
					m02-mb.m02,m12-mb.m12,m22-mb.m22);
	return result;
}

matrix3x3f& matrix3x3f::operator=(real32 val)
{
	set(val);
	return *this;
}

matrix3x3f& matrix3x3f::operator*=(real32 val)
{
	m00*=val;m01*=val;m02*=val;
	m10*=val;m11*=val;m12*=val;
	m20*=val;m21*=val;m22*=val;
	return *this;
}

matrix3x3f& matrix3x3f::operator/=(real32 val)
{
	m00/=val;m01/=val;m02/=val;
	m10/=val;m11/=val;m12/=val;
	m20/=val;m21/=val;m22/=val;
	return *this;
}

matrix3x3f matrix3x3f::operator*(real32 val) const
{
	matrix3x3f result;
	result.set(m00*val,m10*val,m20*val,
					m01*val,m11*val,m21*val,
					m02*val,m12*val,m22*val);
	return result;
}

matrix3x3f matrix3x3f::operator/(real32 val) const
{
	matrix3x3f result;
	result.set(m00/val,m10/val,m20/val,
					m01/val,m11/val,m21/val,
					m02/val,m12/val,m22/val);
	return result;
}


vec2f matrix3x3f::operator*(const vec2f& v) const
{
	vec2f result = {v.x*m00+v.y*m01+1.0f*m02,v.x*m10+v.y*m11+1.0f*m12};
	return result;
}

matrix3x3f operator*(real32 val,const matrix3x3f& m)
{
	matrix3x3f result;
	result.set(m.m00*val,m.m10*val,m.m20*val,
					m.m01*val,m.m11*val,m.m21*val,
					m.m02*val,m.m12*val,m.m22*val);
	return result;
}
matrix3x3f operator/(real32 m21, const matrix3x3f& m)
{
	matrix3x3f result;
	result.set(m21/m.m00,m21/m.m10,m21/m.m20,
					m21/m.m01,m21/m.m11,m21/m.m21,
					m21/m.m02,m21/m.m12,m21/m.m22);
	return result;
}
