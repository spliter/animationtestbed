/*
 * box2f.h
 *
 *  Created on: 4 kwi 2015
 *      Author: Miko Kuta
 */

#ifndef BOX2F_H_
#define BOX2F_H_

struct box2f
{
	union
	{
		struct
		{
			vec2f v1;
			vec2f v2;
		};
		struct
		{
			real32 minX;
			real32 minY;
			real32 maxX;
			real32 maxY;
		};
	};

	box2f()
	{
	}

	box2f(const box2f& other)
	{
		v1 = other.v1;
		v2 = other.v2;
	}

	box2f(vec2f v1, vec2f v2)
	{
		this->v1 = v1;
		this->v2 = v2;
	}

	box2f(real32 minX, real32 minY, real32 maxX, real32 maxY)
	{
		set(minX,minY,maxX,maxY);
	}

	box2f(vec2f position, vec2f size, vec2f origin)
	{
		v1 = position - size * (_vec2f(1.0f, 1.0f) - origin);
		v2 = position - size * origin;
	}

	box2f& set(const box2f& other);
	box2f& set(vec2f v1, vec2f v2);
	box2f& set(vec2f position, vec2f size, vec2f origin);
	box2f& set(real32 minX, real32 minY, real32 maxX, real32 maxY);

	real32 getWidth(){return maxX-minX;}
	real32 getHeight(){return maxY-minY;}

	bool32 intersects(const box2f& other) const;
	vec2f getSmallestIntersection(const box2f& other) const; //Note: it assumes intersection exists

	box2f& operator+=(const vec2f& rh);
	box2f& operator-=(const vec2f& rh);

	box2f operator+(const vec2f& rh) const;
	box2f operator-(const vec2f& rh) const;
};



box2f& box2f::set(const box2f& other)
{
	*this = other;
	return *this;
}
box2f& box2f::set(vec2f v1, vec2f v2)
{
	this->v1 = v1;
	this->v2 = v2;
	return *this;
}
box2f& box2f::set(vec2f position, vec2f size, vec2f origin)
{
	v1 = position - size * (_vec2f(1.0f, 1.0f) - origin);
	v2 = position - size * origin;
	return *this;
}

box2f& box2f::set(real32 minX, real32 minY, real32 maxX, real32 maxY)
{
	this->minX = minX;
	this->minY = minY;
	this->maxX = maxX;
	this->maxY = maxY;
	return *this;
}

inline bool32 box2f::intersects(const box2f& other) const
								{
	bool32 result = other.maxX >= minX && other.minX <= maxX && other.maxY >= minY && other.minY <= maxY;
	return result;
}

inline vec2f box2f::getSmallestIntersection(const box2f& other) const
											{
	vec2f result = {};

	real32 dif;
	real32 smallestDif = other.maxX - minX;
	result.set(-smallestDif, 0.0f);

	dif = other.minX - maxX;
	if (dif < smallestDif)
	{
		smallestDif = dif;
		result.set(smallestDif, 0.0f);
	}

	dif = other.maxX - minX;
	if (dif < smallestDif)
	{
		smallestDif = dif;
		result.set(0.0f, -smallestDif);
	}

	dif = other.minY - maxY;
	if (dif < smallestDif)
	{
		smallestDif = dif;
		result.set(0.0f, smallestDif);
	}
	return result;
}

inline box2f& box2f::operator+=(const vec2f& rh)
{
	v1 += rh;
	v2 += rh;
	return *this;
}

inline box2f& box2f::operator-=(const vec2f& rh)
{
	v1 -= rh;
	v2 -= rh;
	return *this;
}


inline box2f box2f::operator+(const vec2f& rh) const
								{
	box2f result;
	result.v1 = v1 + rh;
	result.v2 = v2 + rh;
	return result;
}

inline box2f box2f::operator-(const vec2f& rh) const
								{
	box2f result;
	result.v1 = v1 - rh;
	result.v2 = v2 - rh;
	return result;
}

#endif
