/*
 * types.h
 *
 *  Created on: 28 gru 2014
 *      Author: Miko Kuta
 */

#pragma once


typedef __int8 int8;
typedef __int16 int16;
typedef __int32 int32;
typedef __int64 int64;

typedef unsigned __int8 uint8;
typedef unsigned __int16 uint16;
typedef unsigned __int32 uint32;
typedef unsigned __int64 uint64;

typedef int32 bool32;

typedef float real32;
typedef double real64;

typedef unsigned int uint;
