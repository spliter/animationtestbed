/*
 * game_utils.h
 *
 *  Created on: 31 mar 2015
 *      Author: Miko Kuta
 */

#ifndef GAME_UTILS_H_
#define GAME_UTILS_H_

#include "float.h"
#include <stdio.h>
#include "../math/vec3f.h"
#include "../math/quaternionf.h"
#include "../math/matrix4x4f.h"


struct game_camera_2d
{
	vec2f pos;
	real32 zoom;
	vec2f anchor;
	real32 rotationDeg;

	vec2f cameraToWorldSpace(real32 x, real32 y);
	real32 cameraToWorldRotation(real32 rotationDeg);
};

struct game_camera_3d
{
	vec3f pos;
	real32 rotPitch,rotYaw;
	real32 fov;
	real32 nearClip, farClip;

	matrix4x4f getFrustumMatrix(real32 widthOverHeight);
	matrix4x4f getInvTransform();
	quaternionf getRotationQuat();
	vec3f cameraToWorldSpace(real32 x, real32 y);
	quaternionf cameraToWorldRotation(quaternionf rot);
	vec3f getForward();
	vec3f getRight();
	vec3f getUp();
	//TODO: worldToCameraSpace and worldToCameraRotation
};

inline bool32 isInside(int32 testX, int32 testY, int32 rectX1, int32 rectY1, int32 rectX2, int32 rectY2)
{
	bool32 result = false;
	if (rectX1 <= testX && rectX2 > testX &&
		rectY1 <= testY && rectY2 > testY)
	{
		result = true;
	}
	return result;
}

inline matrix4x4f game_camera_3d::getFrustumMatrix(real32 widthOverHeight)
{
	real32 top = nearClip * tan(degToRad(fov / 2));
	real32 right = top * widthOverHeight;
	real32 left = -right;
	real32 bottom = -top;

	real32 w = right-left;
	real32 h = top-bottom;
	real32 nearclip2 = nearClip*2;
	matrix4x4f result;

	real32 a = nearclip2 / w;
	real32 b = nearclip2 / h;
	real32 c = 0;//(right+left)/w;
	real32 d = 0;//(top+bottom)/h;
	real32 e = -(farClip+nearClip)/(farClip-nearClip);
	real32 f = -(2*farClip*nearClip)/(farClip-nearClip);

	result.m00 = a; result.m01 = 0;	result.m02 = c;		result.m03 = 0;
	result.m10 = 0; result.m11 = b;	result.m12 = d;		result.m13 = 0;
	result.m20 = 0; result.m21 = 0;	result.m22 = e;		result.m23 = f;
	result.m30 = 0; result.m31 = 0;	result.m32 = -1.0f; result.m33 = 0;

	//result.transpose();

	return result;
}

inline matrix4x4f game_camera_3d::getInvTransform()
{
	matrix4x4f result;
	quaternionf rot = getRotationQuat().conjugate();
	result = rot.getRotationMatrix();
	result.translate(-pos);

	return result;
}

inline quaternionf game_camera_3d::getRotationQuat()
{
	quaternionf result = quaternionf::identity();
	result.rotateByDeg(rotYaw,_vec3f(0,1,0));
	result.rotateByDeg(rotPitch,_vec3f(1,0,0));
	return result;
}

inline vec3f game_camera_3d::cameraToWorldSpace(real32 x, real32 y)
{
	vec3f result = { };
	real32 scale = tan(degToRad(fov*0.5f))*nearClip;

	result.set(x*scale,y*scale,-nearClip);
	quaternionf rot = getRotationQuat();
	result = rot * result;
	result += pos;
	return result;
}

inline quaternionf game_camera_3d::cameraToWorldRotation(quaternionf rotation)
{
	quaternionf rot = getRotationQuat();
	quaternionf result = rot*rotation;
	return result;
}


inline vec3f game_camera_3d::getForward()
{
	quaternionf rot = getRotationQuat();
	return rot*vec3f{0.0f,0.0f,-1.0f};
}

inline vec3f game_camera_3d::getRight()
{
	quaternionf rot = getRotationQuat();
	return rot*vec3f{1.0f,0.0f,0.0f};
}

inline vec3f game_camera_3d::getUp()
{
	quaternionf rot = getRotationQuat();
	return rot*vec3f{0.0f,1.0f,0.0f};
}
#endif
