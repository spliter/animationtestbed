/*
 * memory_manager.h
 *
 *  Created on: 21 May 2016
 *      Author: MikoKuta
 */

#pragma once
#include "types.h"
#include "utils.h"
struct stack_memory_manager
{
	uint8* memory;
	uint64 memorySize;
	uint64 memoryUsed;

	void* push(uint32 byteCount, uint8 align=4)
	{
		if(byteCount==0)
		{
			return NULL;
		}
		uint8* result=0;
		uint32 offset = (align-((uint64(memory)+memoryUsed))%align)%align;
		uint64 pushSize = byteCount+offset+sizeof(uint32);

		Assert(memoryUsed+pushSize<memorySize && pushSize<(uint64)0xffffffff);

		result = memory+(memoryUsed+offset);
		memoryUsed+=pushSize;
		uint8* allocInfo = (uint8*)(memory + memoryUsed-sizeof(uint32));
		allocInfo[0] = ((uint8*)&pushSize)[0];
		allocInfo[1] = ((uint8*)&pushSize)[1];
		allocInfo[2] = ((uint8*)&pushSize)[2];
		allocInfo[3] = ((uint8*)&pushSize)[3];

		return result;
	}

	void pop()
	{
		if(memoryUsed>0)
		{
			uint32 pushSize;
			uint8* allocInfo = memory+memoryUsed-sizeof(uint32);
			((uint8*)&pushSize)[0] = allocInfo[0];
			((uint8*)&pushSize)[1] = allocInfo[1];
			((uint8*)&pushSize)[2] = allocInfo[2];
			((uint8*)&pushSize)[3] = allocInfo[3];
			memoryUsed-=pushSize;
		}
	}

	void clear(){memoryUsed=0;}
};

#define pushStruct(manager,struct) (struct*)((manager)->push(sizeof(struct),__alignof(struct)))
#define pushStructArray(manager,struct, count) (struct*)((manager)->push(sizeof(struct)*count,__alignof(struct)))
#define pushStructAlign(manager,struct, align) (struct*)((manager)->push(sizeof(struct),align))
#define pushStructArrayAlign(manager,struct, count, align) (struct*)((manager)->push(sizeof(struct)*count,align))

