/*
 * profiler.h
 *
 *  Created on: 20 Mar 2017
 *      Author: MikoKuta
 */
#pragma once

#include "types.h"
#include "utils.h"
#include "platform_utils.h"
#include "safe_string.h"

struct profiler_entry
{
	uint64 startTicks;
	uint64 endTicks;
	profiler_entry* parent;
	profiler_entry* childHead;
	profiler_entry* childTail;
	profiler_entry* nextSibling;
};

#define MAX_PROFILER_ENTRIES 1024
#define MAX_PROFILER_ENTRY_NAME_LENGTH 64
typedef char profiler_entry_name[MAX_PROFILER_ENTRY_NAME_LENGTH];

struct profiler_state
{
	profiler_entry* lastActiveEntry;
	uint32 entryCount;
	profiler_entry entries[MAX_PROFILER_ENTRIES];
	profiler_entry_name entryNames[MAX_PROFILER_ENTRIES];

	profiler_state()
	{
		memset(entries, 0, sizeof(entries));
		memset(entryNames, 0, sizeof(entryNames));
		entryCount = 0;
		lastActiveEntry = NULL;
		strcpy_n(entryNames[0], MAX_PROFILER_ENTRY_NAME_LENGTH, "Frame");
	}

	void beginFrame()
	{
		lastActiveEntry = entries;
		entryCount = 1;
		entries[0].endTicks = 0;
		entries[0].childHead = NULL;
		entries[0].childTail = NULL;
		entries[0].nextSibling = NULL;
		memset(entries + 1, 0, sizeof(entries) - sizeof(entries[0])); //we clear everything here so during the frame the operations are faster
		entries[0].startTicks = getTicks(); //we set this last to better measure performance of the actual code
	}

	void endFrame()
	{
		entries[0].endTicks = getTicks();
	}

	profiler_entry* pushEntry(const char* entryName)
	{
		if (entryCount < MAX_PROFILER_ENTRIES)
		{
			uint32 entryId = entryCount;
			entryCount++;
			strcpy_n(entryNames[entryId], MAX_PROFILER_ENTRY_NAME_LENGTH, entryName);
			profiler_entry* entry = entries + entryId;

			if (lastActiveEntry->childHead == NULL)
			{
				lastActiveEntry->childHead = entry;
				lastActiveEntry->childTail = entry;
			}
			else
			{
				lastActiveEntry->childTail->nextSibling = entry;
				lastActiveEntry->childTail = entry;
			}

			entry->parent = lastActiveEntry;
			lastActiveEntry = entry;
			/*
			 * Note: Miko: all other entry() fields are already set to NULL in beginFrame()
			 */

			return entry;
		}
		return NULL;
	}

	void popEntry()
	{
		if (lastActiveEntry == entries) //we cannot pop Frame
		{
			printf("ERROR: ATTEMPTED TO POP \"Frame\" profiling entry");
			return;
		}

		lastActiveEntry = lastActiveEntry->parent;
	}

	profiler_entry* findEntry(const char* name)
	{
		for (uint32 entryIndex = 0; entryIndex < entryCount; entryIndex++)
		{
			if (strcmp(entryNames[entryIndex], name) == 0)
			{
				return entries + entryIndex;
			}
		}
		return NULL;
	}

	uint64 getTotalEntryTicks(const char* name)
	{
		uint64 totalTickCount = 0;
		for (uint32 entryIndex = 0; entryIndex < entryCount; entryIndex++)
		{
			if (strcmp(entryNames[entryIndex], name) == 0)
			{
				totalTickCount = entries[entryIndex].endTicks - entries[entryIndex].startTicks;
			}
		}
		return totalTickCount;
	}
};

struct profiler_block
{
	profiler_entry* myEntry;
	profiler_state* profiler;
	profiler_block(profiler_state* profiler, const char* name)
	{
		this->profiler = profiler;
		myEntry = profiler->pushEntry(name);
		if (myEntry)
		{
			myEntry->startTicks = getTicks();
		}
	}
	~profiler_block()
	{
		if (myEntry)
		{
			myEntry->endTicks = getTicks();
			profiler->popEntry();
		}
	}
};
