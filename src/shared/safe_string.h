/*
 * safe_string.h
 *
 *  Created on: 18 Mar 2017
 *      Author: MikoKuta
 */

#pragma once

//strcpy_n
//returns how many bytes it wrote, including the terminal 0
size_t strcpy_n(char* dst, size_t dstLen, const char* src)
{
	if (!src || !dst || dstLen == 0)
	{
		return 0;
	}

	size_t freeLen = dstLen - 1;

	while (*src && freeLen)
	{
		*dst = *src;
		++src;
		++dst;
		--freeLen;
	}
	*dst = 0;

	return dstLen - freeLen;
}
