/*
 * game_common.h
 *
 *  Created on: 25 gru 2014
 *      Author: Miko Kuta
 */

#pragma once
#include "types.h"
#include "string.h"

#define Assert(condition) if(!(condition)){*((int*)(0))=0;}

#define Kilobytes(n) ((n)*1024L)
#define Megabytes(n) (Kilobytes(n)*1024L)
#define Gigabytes(n) (Megabytes(n)*1024L)

#define ToKilobytes(n) ((n)/1024L)
#define ToMegabytes(n) (ToKilobytes(n)/1024L)
#define ToGigabytes(n) (ToMegabytes(n)/1024L)

#define STRINGIZEEX(value) #value
#define STRINGIZE(value) STRINGIZEEX(value)
#define APPEND_PROTECTED_AUX(x,y) x ## y
#define APPEND_PROTECTED(x,y) APPEND_PROTECTED_AUX(x,y)

#define MACRO_PROTECT(...) __VA_ARGS__

#define local static
#define global_variable static
#define persistent static

#define ZERO_STRUCT(struct_class,ptr) memset(ptr,0,sizeof(struct_class))
#define ZERO_STRUCT_ARRAY(struct_class, ptr, count) memset(ptr,0,sizeof(struct_class)*count)
#define ZERO_VARIABLE(var) memset(&var,0,sizeof(var))

#define AlignCastPtr(memoryBlock, type)(type*)((uint64)memoryBlock  + (uint64)memoryBlock % __alignof(type))

#ifndef NULL
#define NULL 0
#endif

#define ARRAY_LENGTH(array) (sizeof(array)/sizeof(array[0]))

//Note(miko): To use ABORT_IF, define ABORT_OP() with the code you want executed to abort your current procedure
#define ABORT_IF(x) if(x){printf("%s:%s failed test: \"%s\", aborting",GetFilename(__FILE__),STRINGIZE(__LINE__),STRINGIZE(x));ABORT_OP();}
