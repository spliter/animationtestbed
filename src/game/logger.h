/*
 * logger.h
 *
 *  Created on: 3 Oct 2017
 *      Author: MikoKuta
 */

#pragma once

#include "shared/types.h"
#include "shared/utils.h"
#include "shared/memory_manager.h"
#include "math/mathutils.h"
#include <stdarg.h>
#include <GL/gl.h>
#include <GL/glu.h>

#define MAX_TEMP_LOG_LINE_LENGTH 2048
#define MAX_LOG_LINE_LENGTH 256
typedef char log_line[MAX_LOG_LINE_LENGTH];

struct debug_logger
{
	log_line *lines;
	char *tempLogLine; //large buffer so we can break the line up if needed
	uint32 lineStart;
	uint32 lineEnd;
	uint32 lineCount;
	bool windowOpen;
	bool hasAddedLog;

	void init(uint32 maxLineCount, stack_memory_manager* memory)
	{
		uint64 prevUsed = memory->memoryUsed;
		tempLogLine = pushStructArray(memory, char, MAX_TEMP_LOG_LINE_LENGTH);
		lines = pushStructArray(memory, log_line, maxLineCount);
		lineStart = 0;
		lineEnd = 0;
		windowOpen = false;
		hasAddedLog = false;
		if (tempLogLine && lines)
		{
			lineCount = maxLineCount;
			memset(tempLogLine, 0, sizeof(char) * MAX_TEMP_LOG_LINE_LENGTH);
			memset(lines, 0, sizeof(log_line) * maxLineCount);
		}
		else
		{
			lineCount = 0;
			memory->memoryUsed = prevUsed;
		}
	}

	void log(const char* format, ...)
	{
		va_list args;

		va_start(args,format);
		int32 writtenLength = vsnprintf_s(tempLogLine, MAX_TEMP_LOG_LINE_LENGTH, _TRUNCATE, format, args);
		int32 lengthLeft = writtenLength;
		char* logLine = tempLogLine;

		while (lengthLeft > 0)
		{
			//TODO: calculate new head and tail
			uint32 lineLength = mini(lengthLeft, MAX_LOG_LINE_LENGTH-1);
			memcpy(lines[lineEnd], logLine, lineLength);
			printf(lines[lineEnd]);
			printf("\n");
			lines[lineEnd][lineLength] = 0;
			lineEnd = (lineEnd + 1) % lineCount;
			lengthLeft -= lineLength;
			if (lineEnd == lineStart)
			{
				lineStart = (lineStart + 1) % lineCount;
			}
		}
		hasAddedLog = true;
		va_end(args);
	}

	void logOpenGLError(const char* tag = NULL)
	{
		GLenum error = GL_NO_ERROR;
		while ((error = glGetError()) != GL_NO_ERROR)
		{
			log("OpenGL Error: %s at %s\n", gluErrorString(error), tag);
		}
	}

	void display(window_state window)
	{
		ImGui::SetNextWindowPos(ImVec2(0.0f, 0.0f), ImGuiSetCond_Always);
		ImGui::SetNextWindowSize(ImVec2((real32) window.width, (real32) 100), ImGuiSetCond_Once);
		ImGui::PushStyleVar(ImGuiStyleVar_WindowMinSize, ImVec2((real32) window.width, 100.0f));
		if (ImGui::Begin("Log", &windowOpen, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoBringToFrontOnFocus))
		{
			ImVec2 winSize = ImGui::GetWindowSize();
			winSize.x = (real32) window.width;
			if (winSize.y > window.height * 0.5f)
			{
				winSize.y = window.height * 0.5f;
			}
			ImGui::SetWindowSize(winSize, ImGuiSetCond_Always);

			for (uint32 curPos = lineStart; curPos != lineEnd; curPos = (curPos + 1) % lineCount)
			{
				ImGui::TextUnformatted(lines[curPos]);
			}
			if (hasAddedLog)
			{
				hasAddedLog = false;
				ImGui::SetScrollHere();
			}
		}
		ImGui::End();
		ImGui::PopStyleVar(1);
	}
};
