/*
 * debug_renderer.h
 *
 *  Created on: 3 Oct 2017
 *      Author: MikoKuta
 */
#pragma once


#include "shared/types.h"
#include "shared/utils.h"
#include "shared/memory_manager.h"
#include "math/mathutils.h"
#include <GL/gl.h>

struct debug_renderer
{
	vec3f *vertices;
	vec4f *colors;

	vec3f *pointVertices;
	vec4f *pointColors;

	vec3f *lineVertices;
	vec4f *lineColors;

	vec3f *triangleVertices;
	vec4f *triangleColors;

	uint32 pointIndex;
	uint32 lineIndex;
	uint32 triangleIndex;

	uint32 pointOffset;
	uint32 lineOffset;
	uint32 triangleOffset;

	uint32 maxElements;

	void addPoint(vec3f vert, vec4f color = vec4f(1.0f, 0.0f, 0.0f, 1.0f))
	{
		pointVertices[pointIndex] = vert;
		pointColors[pointIndex] = color;
		pointIndex = (pointIndex + 1) % maxElements;
	}

	void addLine(vec3f vert1, vec3f vert2, vec4f color = vec4f(1.0f, 0.0f, 0.0f, 1.0f))
	{
		lineVertices[lineIndex * 2] = vert1;
		lineVertices[lineIndex * 2 + 1] = vert2;
		lineColors[lineIndex * 2] = color;
		lineColors[lineIndex * 2 + 1] = color;
		lineIndex = (lineIndex + 1) % maxElements;
	}

	void addTriangle(vec3f vert1, vec3f vert2, vec3f vert3, vec4f color = vec4f(1.0f, 0.0f, 0.0f, 1.0f))
	{
		triangleVertices[triangleIndex * 3] = vert1;
		triangleVertices[triangleIndex * 3 + 1] = vert2;
		triangleVertices[triangleIndex * 3 + 2] = vert2;
		triangleColors[triangleIndex * 3] = color;
		triangleColors[triangleIndex * 3 + 1] = color;
		triangleColors[triangleIndex * 3 + 2] = color;
		triangleIndex = (triangleIndex + 1) % maxElements;
	}

	bool32 setup(uint32 maxElementsPerType, stack_memory_manager *memory)
	{
		uint64 oldMemoryUsed = memory->memoryUsed;

		vertices = pushStructArray(memory, vec3f, maxElementsPerType * 6);
		colors = pushStructArray(memory, vec4f, maxElementsPerType * 6);

		if (vertices && colors)
		{
			pointOffset = 0;
			lineOffset = maxElementsPerType;
			triangleOffset = lineOffset + maxElementsPerType * 2;

			pointVertices = vertices + pointOffset;
			pointColors = colors + pointOffset;

			lineVertices = vertices + lineOffset;
			lineColors = colors + lineOffset;

			triangleVertices = vertices + triangleOffset;
			triangleColors = colors + triangleOffset;

			maxElements = maxElementsPerType;
			clear();
			return true;
		}
		else
		{
			memory->memoryUsed = oldMemoryUsed;
			maxElements = 0;
			vertices = 0;
			colors = 0;
			return false;
		}
	}
	void clear()
	{
		memset(vertices, 0, sizeof(vec3f) * maxElements * 6);
		memset(colors, 0, sizeof(vec4f) * maxElements * 6);
	}

	void render()
	{
		glLineWidth(2.0f);
		glPointSize(5.0f);

		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_COLOR_ARRAY);

		glVertexPointer(3, GL_FLOAT, 0, vertices);
		glColorPointer(4, GL_FLOAT, 0, colors);
		glDrawArrays(GL_POINTS, pointOffset, maxElements);
		glDrawArrays(GL_LINES, lineOffset, maxElements * 2);
		glDrawArrays(GL_TRIANGLES, triangleOffset, maxElements * 3);

		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);

		glLineWidth(1.0f);
		glPointSize(1.0f);
	}
};
