/*
 * Texturemanager.h
 *
 *  Created on: 2 Feb 2017
 *      Author: MikoKuta
 */
#pragma once

#include "shared/types.h"
#include "shared/utils.h"
#include "shared/memory_manager.h"


#define MAX_TEXTURE_FILENAME_LENGTH 1024
#define INVALID_TEXTURE_HANDLE 0
typedef uint32 texture_handle;
struct texture_native_info;

typedef char texture_filename[MAX_TEXTURE_FILENAME_LENGTH];

struct texture_info
{
	texture_handle handle;
	int width,height;
};



struct am_texture_manager
{
	static am_texture_manager* create(uint32 maxTextures, stack_memory_manager* manager, uint32 textureMemorySize);
	void clear(am_texture_manager* texManager);
	texture_info getTexture(const char* name, bool32 loadIfNotLoaded = true, bool32 unique = false);
	void unloadTexture(uint32 handle);

	void bindTexture(uint32 handle);//use 0 to unbind completely

	void reloadTextures();

	stack_memory_manager memory;
	texture_native_info* textures;
	texture_filename* filenames;//separate because size of each
	uint32 textureCount;
};


