/*
 * app.cpp
 *
 *  Created on: 16 Oct 2016
 *      Author: MikoKuta
 */
#include <gl/GL.h>
#include <cstdlib>
#include <cstdio>
#include <cerrno>
#include <cmath>
#include "game.h"
#include "logger.h"
#include "debug_renderer.h"
#include "resources/am_texture_manager.h"
#include "resources/am_model_manager.h"
#include "resources/am_model.h"

#include "../imgui/imgui.h"
#include "../pugixml/pugixml.hpp"
#include "../shared/camera.h"
#include "../shared/types.h"
#include "../shared/utils.h"
#include "../shared/memory_manager.h"
#include "../shared/platform_utils.h"
#include "../shared/profiler.h"
#include "../shared/safe_string.h"
#include "../math/vec3f.h"
#include "../math/quaternionf.h"
#include "../math/matrix4x4f.h"
#include "../math/vec_math_utils.h"

#include <IL/il.h>
#include <IL/ilut.h>

//Compilation unit
#include "resources/am_texture_manager.cpp"
#include "resources/am_model.cpp"
#include "resources/am_model_manager.cpp"

/*
 * Utilities
 */

struct temp_memory_section
{
	uint64 oldMemoryUsed;
	stack_memory_manager* tempMemory;

	temp_memory_section(stack_memory_manager* memory)
	{
		tempMemory = memory;
		oldMemoryUsed = memory->memoryUsed;
	}

	~temp_memory_section()
	{
		tempMemory->memoryUsed = oldMemoryUsed;
	}
};

#define TEMP_MEMORY_BLOCK(memory) temp_memory_section APPEND_PROTECTED(temp_memory_section_store,__COUNTER__)(memory)

#define PROFILE_BLOCK(name) profiler_block APPEND_PROTECTED(temp_profiler_block,__COUNTER__)(&sProfiler,name)

profiler_state sProfiler;


struct game_state
{
	bool32 isInitialized;
	stack_memory_manager memory;

	stack_memory_manager resourceMemory;
	stack_memory_manager tempMemory;

	game_camera_3d camera;

	am_model* model;

	int32 animClipIndex;
	am_anim_clip_info_packed selectedAnimInfo;
	real32 animRatio;

	debug_renderer debugRenderer;
	debug_logger debugLogger;

	am_texture_manager* textureManager;
};


void SIMD_mulMatrix(__m128 *dstMat, const __m128 *mat, float multiplier)
{
//Note: matrix has to be 16 byte aligned

	__m128 simdMultiplier = _mm_set_ps1(multiplier);

	dstMat[0] = _mm_mul_ps(mat[0], simdMultiplier);
	dstMat[1] = _mm_mul_ps(mat[1], simdMultiplier);
	dstMat[2] = _mm_mul_ps(mat[2], simdMultiplier);
	dstMat[3] = _mm_mul_ps(mat[3], simdMultiplier);
}

void SIMD_addMatrix(__m128* dstMat, const __m128* matA, const __m128 *matB)
{
	dstMat[0] = _mm_add_ps(matA[0],matB[0]);
	dstMat[1] = _mm_add_ps(matA[1],matB[1]);
	dstMat[2] = _mm_add_ps(matA[2],matB[2]);
	dstMat[3] = _mm_add_ps(matA[3],matB[3]);
}

void SIMD_addMulMatrix(__m128 *dstMat, const __m128 *matA, const __m128 *matB, float multiplierB)
{
	__m128 simdMultiplier = _mm_set_ps1(multiplierB);

	dstMat[0] = _mm_add_ps(matA[0], _mm_mul_ps(matB[0], simdMultiplier));
	dstMat[1] = _mm_add_ps(matA[1], _mm_mul_ps(matB[1], simdMultiplier));
	dstMat[2] = _mm_add_ps(matA[2], _mm_mul_ps(matB[2], simdMultiplier));
	dstMat[3] = _mm_add_ps(matA[3], _mm_mul_ps(matB[3], simdMultiplier));
}

void SIMD_mulVector(vec3f* dstVec, const __m128* mat, const vec3f* srcVec)
{
	__m128 simdVecX = _mm_set_ps1(srcVec->x);
	__m128 simdVecY = _mm_set_ps1(srcVec->y);
	__m128 simdVecZ = _mm_set_ps1(srcVec->z);
	__m128 simdVecW = _mm_set_ps1(1.0f);

	__m128 column0 = _mm_mul_ps(mat[0],simdVecX);
	__m128 column1 = _mm_mul_ps(mat[1],simdVecY);
	__m128 column2 = _mm_mul_ps(mat[2],simdVecZ);
	__m128 column3 = _mm_mul_ps(mat[3],simdVecW);

	//_MM_TRANSPOSE4_PS(column0,column1,column2,column3);
	__m128 total = _mm_add_ps(_mm_add_ps(column0,column1), _mm_add_ps(column2,column3));

	dstVec->x = total.m128_f32[0];
	dstVec->y = total.m128_f32[1];
	dstVec->z = total.m128_f32[2];

	/*
	 x = m00*v.x + m01*v.y + m02*v.z + m03,
	 y = m10*v.x + m11*v.y + m12*v.z + m13,
	 z = m20*v.x + m21*v.y + m22*v.z + m23);
	 */
}

void SIMD_skin(__m128 *dstMat, __m128 *mat0, __m128 *mat1, __m128 *mat2, __m128 *mat3, real32 weight0, real32 weight1, real32 weight2, real32 weight3)
{
	__m128 simdMultiplier0 = _mm_set_ps1(weight0);
	__m128 simdMultiplier1 = _mm_set_ps1(weight1);
	__m128 simdMultiplier2 = _mm_set_ps1(weight2);
	__m128 simdMultiplier3 = _mm_set_ps1(weight3);

	/*
	 dstMat[0] = _mm_add_ps(
	 _mm_add_ps(_mm_mul_ps(mat0[0],simdMultiplier0),_mm_mul_ps(mat1[0],simdMultiplier1)),
	 _mm_add_ps(_mm_mul_ps(mat2[0],simdMultiplier0),_mm_mul_ps(mat2[0],simdMultiplier1))
	 );
	 dstMat[1] = _mm_add_ps(
	 _mm_add_ps(_mm_mul_ps(mat0[1],simdMultiplier0),_mm_mul_ps(mat1[1],simdMultiplier1)),
	 _mm_add_ps(_mm_mul_ps(mat2[1],simdMultiplier0),_mm_mul_ps(mat2[1],simdMultiplier1))
	 );
	 dstMat[2] = _mm_add_ps(
	 _mm_add_ps(_mm_mul_ps(mat0[2],simdMultiplier0),_mm_mul_ps(mat1[2],simdMultiplier1)),
	 _mm_add_ps(_mm_mul_ps(mat2[2],simdMultiplier0),_mm_mul_ps(mat2[2],simdMultiplier1))
	 );
	 dstMat[3] = _mm_add_ps(
	 _mm_add_ps(_mm_mul_ps(mat0[3],simdMultiplier0),_mm_mul_ps(mat1[3],simdMultiplier1)),
	 _mm_add_ps(_mm_mul_ps(mat2[3],simdMultiplier0),_mm_mul_ps(mat2[3],simdMultiplier1))
	 );
	 */

	dstMat[0] = _mm_mul_ps(mat0[0], simdMultiplier0);
	dstMat[1] = _mm_mul_ps(mat0[1], simdMultiplier0);
	dstMat[2] = _mm_mul_ps(mat0[2], simdMultiplier0);
	dstMat[3] = _mm_mul_ps(mat0[3], simdMultiplier0);

	dstMat[0] = _mm_add_ps(dstMat[0], _mm_mul_ps(mat1[0], simdMultiplier1));
	dstMat[1] = _mm_add_ps(dstMat[1], _mm_mul_ps(mat1[1], simdMultiplier1));
	dstMat[2] = _mm_add_ps(dstMat[2], _mm_mul_ps(mat1[2], simdMultiplier1));
	dstMat[3] = _mm_add_ps(dstMat[3], _mm_mul_ps(mat1[3], simdMultiplier1));

	dstMat[0] = _mm_add_ps(dstMat[0], _mm_mul_ps(mat2[0], simdMultiplier2));
	dstMat[1] = _mm_add_ps(dstMat[1], _mm_mul_ps(mat2[1], simdMultiplier2));
	dstMat[2] = _mm_add_ps(dstMat[2], _mm_mul_ps(mat2[2], simdMultiplier2));
	dstMat[3] = _mm_add_ps(dstMat[3], _mm_mul_ps(mat2[3], simdMultiplier2));

	dstMat[0] = _mm_add_ps(dstMat[0], _mm_mul_ps(mat3[0], simdMultiplier3));
	dstMat[1] = _mm_add_ps(dstMat[1], _mm_mul_ps(mat3[1], simdMultiplier3));
	dstMat[2] = _mm_add_ps(dstMat[2], _mm_mul_ps(mat3[2], simdMultiplier3));
	dstMat[3] = _mm_add_ps(dstMat[3], _mm_mul_ps(mat3[3], simdMultiplier3));
}

#define ANIMASTER_DEFAULT_UI_SNAP 10
extern float roundf(float);

void snapCurrentImguiWindow(real32 screenWidth, real32 screenHeight, bool32 clampToScreenEdges = true, real32 snap = ANIMASTER_DEFAULT_UI_SNAP)
{
	//TODO: modify imgui window to enable snapping

	ImVec2 pos = ImGui::GetWindowPos();
	ImVec2 size = ImGui::GetWindowSize();

	if (snap > FLT_EPSILON)
	{
		pos.x = roundf(pos.x / snap) * snap;
		pos.y = roundf(pos.y / snap) * snap;

		size.x = roundf(size.x / snap) * snap;
		size.y = roundf(size.y / snap) * snap;
	}

	if (clampToScreenEdges)
	{
		size.x = minf(screenWidth, size.x);
		size.y = minf(screenWidth, size.y);

		pos.x = maxf(0.0f, minf(screenWidth - size.x, pos.x));
		pos.y = maxf(0.0f, minf(screenHeight - size.y, pos.y));
	}

	if (ImGui::IsWindowBeingMoved())
	{
		//TODO: draw a rectangle of where the window will be clipped
		/*
		 ImVec2 min = pos;
		 ImVec2 max=ImVec2(pos.x+size.x,pos.y+size.y);
		 ImDrawList* drawList = ImGui::GetWindowDrawList();
		 drawList->AddCallback()
		 ImVec4 prevClipRect = drawList->_ClipRectStack;
		 drawList->AddRect(min,max,0xffffff80);
		 */
	}
	else
	{
		ImGui::SetWindowPos(pos, ImGuiSetCond_Always);
	}
}

void skinAndDrawModel(am_model* model, matrix4x4f* nodeWorldSpaceMatrices, am_texture_manager* textureManager, stack_memory_manager* tempMemory)
{
	TEMP_MEMORY_BLOCK(tempMemory);
	if (model)
	{
		PROFILE_BLOCK("Drawing Model");
		//todo(miko): vertex skinning
		//printf("Model bone count: %d\n",model->bones.boneCount);
		uint32 matrixCount = model->nodes.nodeCount;
		if (matrixCount)
		{
			glColor3f(1.0f, 1.0f, 1.0f);
			glEnable(GL_CULL_FACE);
			glCullFace(GL_BACK);
			glEnable(GL_TEXTURE_2D);
			uint32 nodeCount = model->nodes.nodeCount;
			am_node_info* nodeInfo = model->nodes.info;
			glEnableClientState(GL_VERTEX_ARRAY);
			glEnableClientState(GL_NORMAL_ARRAY);
			for (uint32 nodeIndex = 0; nodeIndex < nodeCount; nodeIndex++, nodeInfo++)
			{
				if (nodeInfo->meshRefCount)
				{
					glPushMatrix();
					glMultMatrixf(nodeWorldSpaceMatrices[nodeIndex].me);

					uint32 meshRefCount = nodeInfo->meshRefCount;
					uint32* meshRef = model->nodes.meshIds + nodeInfo->meshRefStart;

					for (uint32 meshRefIndex = 0; meshRefIndex < meshRefCount; meshRefIndex++)
					{
						am_mesh* curMesh = model->meshes + *meshRef;
						bool usingTexCoordArray = false;
						textureManager->bindTexture(model->materials[curMesh->material].binding[0].texHandle);
						if (curMesh->uvs[0])
						{
							usingTexCoordArray = true;
							glEnableClientState(GL_TEXTURE_COORD_ARRAY);
						}

						if (curMesh->boneCount)
						{
							PROFILE_BLOCK("Rendering skinned model");
							glPopMatrix();
							matrix4x4f* boneMats = pushStructArrayAlign(tempMemory, matrix4x4f, curMesh->boneCount, 16);

							uint32 boneCount = curMesh->boneCount;
							am_bone* curBone = curMesh->bones;
							matrix4x4f *curBoneMat = boneMats;
							{
								PROFILE_BLOCK("Bind Mat");
								for (uint32 boneId = 0; boneId < boneCount; boneId++, curBone++, curBoneMat++)
								{
									*curBoneMat = nodeWorldSpaceMatrices[curBone->node] * curBone->bindMat;
								}
							}
							Assert(((uint64 )boneMats) % 16 == 0);

							vec3f* vertices = pushStructArray(tempMemory, vec3f, curMesh->vertexCount);

							uint32 vertCount = curMesh->vertexCount;
							vec3f *curVert = vertices;
							vec3f *srcVert = (vec3f*) curMesh->vertices;
							float *curVertWeight = curMesh->vertWeights;
							uint8 *curVertBoneId = curMesh->vertBoneIds;
							{
								__m128 *simdBoneMats = (__m128 *) boneMats;
								__m128 simdTempMat[4];

								PROFILE_BLOCK("Skinning");

								//uint64 start = __rdtsc();
								for (uint32 vertIndex = 0; vertIndex < vertCount; vertIndex++, curVert++, srcVert++/*, curVertWeight += 4, curVertBoneId += 4*/)
								{
									uint32 weightId = vertIndex * 4;

									SIMD_mulMatrix(simdTempMat, (__m128 *) (boneMats + curVertBoneId[weightId + 0]), curVertWeight[weightId + 0]);
									SIMD_addMulMatrix(simdTempMat, simdTempMat, (__m128 *) (boneMats + curVertBoneId[weightId + 1]), curVertWeight[weightId + 1]);
									SIMD_addMulMatrix(simdTempMat, simdTempMat, (__m128 *) (boneMats + curVertBoneId[weightId + 2]), curVertWeight[weightId + 2]);
									SIMD_addMulMatrix(simdTempMat, simdTempMat, (__m128 *) (boneMats + curVertBoneId[weightId + 3]), curVertWeight[weightId + 3]);
									SIMD_mulVector(curVert, simdTempMat, srcVert);

									/*
									 matrix4x4f mat0 = boneMats[curVertBoneId[weightId + 0]] * curVertWeight[weightId + 0];
									 matrix4x4f mat1 = boneMats[curVertBoneId[weightId + 1]] * curVertWeight[weightId + 1];
									 matrix4x4f mat2 = boneMats[curVertBoneId[weightId + 2]] * curVertWeight[weightId + 2];
									 matrix4x4f mat3 = boneMats[curVertBoneId[weightId + 3]] * curVertWeight[weightId + 3];

									 *curVert = (mat0+mat1+mat2+mat3) * *srcVert;
									 */
								}
								//uint64 end = __rdtsc();
								//printf("ticksL %llu\n",end-start);
							}

							{
								PROFILE_BLOCK("Actual rendering");
								glVertexPointer(3, GL_FLOAT, 0, vertices);
								glNormalPointer(GL_FLOAT, 0, curMesh->normals);
								if (usingTexCoordArray)
								{
									glTexCoordPointer(curMesh->uvElementCount[0], GL_FLOAT, 0, curMesh->uvs[0]);
								}
								glDrawElements(GL_TRIANGLES, curMesh->triangleCount * 3, GL_UNSIGNED_INT, curMesh->triangles);
							}

							glPushMatrix();
							glMultMatrixf(nodeWorldSpaceMatrices[nodeIndex].me);

						}
						else
						{
							glVertexPointer(3, GL_FLOAT, 0, curMesh->vertices);
							glNormalPointer(GL_FLOAT, 0, curMesh->normals);
							if (usingTexCoordArray)
							{
								glTexCoordPointer(curMesh->uvElementCount[0], GL_FLOAT, 0, curMesh->uvs[0]);
							}
							glDrawElements(GL_TRIANGLES, curMesh->triangleCount * 3, GL_UNSIGNED_INT, curMesh->triangles);
						}
						if (usingTexCoordArray)
						{
							glDisableClientState(GL_TEXTURE_COORD_ARRAY);
						}
					}
					glPopMatrix();
				}
			}
			textureManager->bindTexture(0);

			glDisableClientState(GL_VERTEX_ARRAY);
			glDisableClientState(GL_NORMAL_ARRAY);
		}
	}
}

void UpdateGame(void* memoryBuffer, uint64 memoryBufferSize, window_state window, input_state* input, uint32 frameDif)
{
	sProfiler.beginFrame();

	game_state *gameState = AlignCastPtr(memoryBuffer, game_state);
	if (!gameState->isInitialized)
	{
		gameState->isInitialized = true;
		gameState->memory.memory = (uint8*) gameState + sizeof(game_state);
		gameState->memory.memorySize = (uint64) memoryBuffer + memoryBufferSize - ((uint64) gameState->memory.memory);

		printf("Memory Size: %d\n", gameState->memory.memorySize);

		gameState->resourceMemory.memory = (uint8*) gameState->memory.push(Megabytes(50), 16);
		gameState->resourceMemory.memorySize = Megabytes(50);
		if (!gameState->resourceMemory.memory)
		{
			ShowError("Not enough memory: Failed to allocate space for resources");
			return;
		}

		gameState->tempMemory.memory = (uint8*) gameState->memory.push(Megabytes(50), 16);
		gameState->tempMemory.memorySize = Megabytes(50);
		if (!gameState->tempMemory.memory)
		{
			ShowError("Not enough memory: Failed to allocate space for temp operations");
			return;
		}

		gameState->textureManager = am_texture_manager::create(1024, &gameState->resourceMemory, Megabytes(25));

		if (!gameState->textureManager)
		{
			ShowError("Not enough memory: Failed to allocate space for texture manager");
			return;
		}

		gameState->camera.farClip = 1000.0f;
		gameState->camera.nearClip = 0.1f;
		gameState->camera.pos.set(0.0f, 0.0f, 3.0f);
		gameState->camera.rotPitch = 0.0f;
		gameState->camera.rotYaw = 0.0f;
		gameState->camera.fov = 90.0f;

		gameState->debugRenderer.setup(30, &gameState->memory);
		gameState->debugLogger.init(100, &gameState->memory);

		gameState->model = amLoadModel("data/dragon.fbx", &gameState->resourceMemory, &gameState->tempMemory, gameState->textureManager);

		matrix4x4f matrix = gameState->camera.getInvTransform();

		gameState->animClipIndex = amFindAnimation(gameState->model->animation.names,gameState->model->animation.animCount,"Idle");

		if(gameState->animClipIndex>=0)
		{
			gameState->selectedAnimInfo = amGetPackedAnimationClipInfo(gameState->model,gameState->animClipIndex);
		}
		gameState->animRatio = 0.0f;
	}

	real32 windowWidth = (real32) window.width;
	real32 windowHeight = (real32) window.height;

	debug_renderer &debug = gameState->debugRenderer;
	debug_logger &logger = gameState->debugLogger;

	TEMP_MEMORY_BLOCK(&gameState->memory);
	real32 dtf = frameDif * 0.001f;
	am_model* model = gameState->model;

	vec2f mousePos = { input->mouse.x, input->mouse.y };

	matrix4x4f* nodeWorldSpaceMatrices = NULL;
	vec3f *nodeWorldSpacePositions = NULL;

	{
		PROFILE_BLOCK("UI");
		ImGuiIO &io = ImGui::GetIO();

		logger.display(window);

		if (ImGui::Begin("Profiler", NULL))
		{
			snapCurrentImguiWindow(windowWidth, windowHeight);
		}
		ImGui::End();
	}

	if (model && model->nodes.nodeCount > 0)
	{
		PROFILE_BLOCK("Blending");

		uint32 matrixCount = model->nodes.nodeCount;

		matrix4x4f* nodeLocalSpaceMatrices = NULL;

		nodeLocalSpaceMatrices = pushStructArrayAlign(&gameState->tempMemory, matrix4x4f, matrixCount, 16);
		nodeWorldSpaceMatrices = pushStructArrayAlign(&gameState->tempMemory, matrix4x4f, matrixCount, 16);

		memcpy(nodeLocalSpaceMatrices, model->nodes.matrices, matrixCount * sizeof(matrix4x4f));

		uint32 nodeCount = model->nodes.nodeCount;
		bool32 useOldTree = false;

		am_transform_buffer finalPoseTransforms;
		finalPoseTransforms.nodeCount = nodeCount;
		finalPoseTransforms.transforms = pushStructArray(&gameState->tempMemory, am_transform, nodeCount);

		for (uint32 nodeIndex = 0; nodeIndex < nodeCount; nodeIndex++)
		{
			finalPoseTransforms.transforms[nodeIndex] = gameState->model->nodes.transforms[nodeIndex];
		}


		if(gameState->animClipIndex>=0)
		{
			float animLength = (real32)gameState->selectedAnimInfo.frameCount / (real32)gameState->selectedAnimInfo.fps;

			real32 animRatio = 0.0f;
			if(animLength>0.0f)
			{
				gameState->animRatio += dtf / gameState->selectedAnimInfo.fps;
				gameState->animRatio = fmodf(gameState->animRatio,1.0f);
			}

			amCalculateAnimatedPoseIntoBuffer(&finalPoseTransforms,&gameState->selectedAnimInfo,gameState->animRatio);
		}

		for (uint32 nodeIndex = 0; nodeIndex < nodeCount; nodeIndex++)
		{
			nodeLocalSpaceMatrices[nodeIndex].makeIdentity();
			nodeLocalSpaceMatrices[nodeIndex].translate(finalPoseTransforms.transforms[nodeIndex].pos);
			nodeLocalSpaceMatrices[nodeIndex] *= finalPoseTransforms.transforms[nodeIndex].rot.getRotationMatrix();
			nodeLocalSpaceMatrices[nodeIndex].scale(finalPoseTransforms.transforms[nodeIndex].scale);
		}

		nodeWorldSpaceMatrices[0] = nodeLocalSpaceMatrices[0];
		//nodeWorldSpaceMatrices[0].scale(0.01f);
		for (uint32 boneIndex = 1; boneIndex < model->nodes.nodeCount; boneIndex++)
		{
			uint32 parent = model->nodes.info[boneIndex].parent;
			nodeWorldSpaceMatrices[boneIndex] = nodeWorldSpaceMatrices[parent] * nodeLocalSpaceMatrices[boneIndex];
		}

		nodeWorldSpacePositions = pushStructArray(&gameState->tempMemory, vec3f, nodeCount);

		vec3f nilVec = vec3f(0.0f, 0.0f, 0.0f);

		for (uint32 nodeIndex = 0; nodeIndex < nodeCount; nodeIndex += 1)
		{
			SIMD_mulVector(nodeWorldSpacePositions + nodeIndex, (__m128 *) (nodeWorldSpaceMatrices + nodeIndex), &nilVec);
		}
	}

	real32 screenRatio = 1.0f;

	if (window.height != 0 && window.width != 0)
	{
		screenRatio = (real32) window.width / (real32) window.height;
	}
	vec2f mouseCameraPos = { (mousePos.x / window.width) * screenRatio * 2.0f - 1.0f * screenRatio, ((window.height - mousePos.y) / window.height) * 2.0f - 1.0f };
	vec3f cameraRayOrigin = gameState->camera.cameraToWorldSpace(mouseCameraPos.x, mouseCameraPos.y);
	vec3f cameraRayDirection = (cameraRayOrigin - gameState->camera.pos).normalize();

	if (!ImGui::GetIO().WantCaptureMouse)
	{
		if (input->button.camera.isDown)
		{
			real32 sensitivity = 0.5f;
			gameState->camera.rotPitch -= (input->mouse.y - input->mouse.prevY) * sensitivity;
			gameState->camera.rotYaw -= (input->mouse.x - input->mouse.prevX) * sensitivity;

			gameState->camera.rotPitch = clamp(-90, 90, gameState->camera.rotPitch);
		}
	}

	real32 camSpeed = 10.0f; //m/s
	vec3f camForward = gameState->camera.getForward();
	vec3f camRight = gameState->camera.getRight();
	vec3f camUp = gameState->camera.getUp();

	if (!ImGui::GetIO().WantCaptureKeyboard)
	{
		if (input->button.back.isPressed())
		{
			gameState->camera.pos -= camForward * dtf * camSpeed;
		}
		if (input->button.forward.isPressed())
		{
			gameState->camera.pos += camForward * dtf * camSpeed;
		}
		if (input->button.left.isPressed())
		{
			gameState->camera.pos -= camRight * dtf * camSpeed;
		}
		if (input->button.right.isPressed())
		{
			gameState->camera.pos += camRight * dtf * camSpeed;
		}
		if (input->button.down.isPressed())
		{
			gameState->camera.pos -= camUp * dtf * camSpeed;
		}
		if (input->button.up.isPressed())
		{
			gameState->camera.pos += camUp * dtf * camSpeed;
		}
	}

	if (window.width != 0 && window.height != 0)
	{
		logger.logOpenGLError("Render begin");
		PROFILE_BLOCK("RENDER");

		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);
		glViewport(0, 0, window.width, window.height);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		float widthRatio = (float) window.width / (float) window.height;
		//glFrustum(-right, right, -top, top, appState->camera.nearClip, appState->camera.farClip);
		matrix4x4f frustumMatrix = gameState->camera.getFrustumMatrix(widthRatio);
		glLoadMatrixf(frustumMatrix.me);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glClearColor(0.5f, 0.5f, 0.6f, 1.0f);
		glClearDepth(1.0f);
		glClearStencil(0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

		//glTranslatef(0.0,0.0,-3.0f);
		matrix4x4f camMat = gameState->camera.getInvTransform();
		glLoadMatrixf(camMat.me);

		glBegin(GL_LINES);

		vec3f ovec = { 0.0f, 0.0f, 0.0f };
		vec3f xvec = { 1.0f, 0.0f, 0.0f };
		vec3f yvec = { 0.0f, 1.0f, 0.0f };
		vec3f zvec = { 0.0f, 0.0f, 1.0f };

		glColor3fv(xvec.me);
		glVertex3fv(ovec.me);
		glVertex3fv(xvec.me);

		glColor3fv(yvec.me);
		glVertex3fv(ovec.me);
		glVertex3fv(yvec.me);

		glColor3fv(zvec.me);
		glVertex3fv(ovec.me);
		glVertex3fv(zvec.me);
		glEnd();

		skinAndDrawModel(model, nodeWorldSpaceMatrices, gameState->textureManager, &gameState->tempMemory);



		glClear(GL_DEPTH_BUFFER_BIT);

		/*
		 * Draw overlay
		 */
		vec3f camera1UnitOffsetRay = cameraRayOrigin - cameraRayDirection * 4;
		glColor3f(1.0f, 1.0f, 1.0f);
		logger.logOpenGLError("Draw mouse axis");

		glDisable(GL_DEPTH_TEST);
		logger.logOpenGLError("Disable depth");

		debug.render();

		logger.logOpenGLError("Debug render");

		glViewport(0, 0, window.width, window.height);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, window.width, window.height, 0, -1.0f, 1.0f);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		logger.logOpenGLError("Setup 2D");

		glColor3f(1.0f, 0.0f, 0.0f);
		glBegin(GL_LINES);
		glVertex2f(mousePos.x - 10, mousePos.y);
		glVertex2f(mousePos.x + 10, mousePos.y);

		glVertex2f(mousePos.x, mousePos.y - 10);
		glVertex2f(mousePos.x, mousePos.y + 10);
		glEnd();

		glColor3f(1.0f, 1.0f, 1.0f);
		logger.logOpenGLError("Draw mouse pos");
	}

	gameState->tempMemory.clear();

	sProfiler.endFrame();

	glFinish();

	if (ImGui::Begin("Profiler"))
	{
		snapCurrentImguiWindow(windowWidth, windowHeight);
		for (uint32 entryIndex = 0; entryIndex < sProfiler.entryCount; entryIndex++)
		{
			real64 elapsedMillis = (sProfiler.entries[entryIndex].endTicks - sProfiler.entries[entryIndex].startTicks) / (getCPUFrequency() / 1000.0);
			ImGui::Text("%s ms: %f\n", sProfiler.entryNames[entryIndex], (real32) elapsedMillis);
		}
	}
	ImGui::End();
	logger.logOpenGLError("Frame End");
	//printf("\n\n\n\n");
}

