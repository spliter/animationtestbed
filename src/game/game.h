/*
 * app.h
 *
 *  Created on: 16 Oct 2016
 *      Author: MikoKuta
 */

#pragma once

#include "../shared/types.h"
#include "../shared/utils.h"

struct input_button_state
{
	int32 switchCount;
	bool32 isDown;

	inline bool32 wasPressed()
	{
		bool32 result = false;
		result = isDown && switchCount>0;
		return result;
	}

	inline bool32 isPressed()
	{
		bool32 result = false;
		result = isDown;
		return result;
	}

	inline bool32 wasReleased()
	{
		bool32 result = false;
		result = !isDown && switchCount>0;
		return result;
	}
};

struct input_state
{
	struct
	{
		input_button_state forward;
		input_button_state back;
		input_button_state left;
		input_button_state right;
		input_button_state up;
		input_button_state down;
		input_button_state camera;
		input_button_state select;
	} button;

	struct
	{
		real32 x,y;
		real32 prevX,prevY;
	} mouse;

	void resetSwitchCounts()
	{
		input_button_state *cur = &button.forward;
		int count = sizeof(button)/sizeof(input_button_state);
		for(int i=0;i<count;++i,++cur)
		{
			cur->switchCount = 0;
		}
	}
};

struct window_state
{
	int x,y,width,height;
};

void UpdateGame(void* memoryBuffer, uint64 memoryBufferSize, window_state window, input_state* input, uint32 frameDif);
void ShowError(const char* text);
