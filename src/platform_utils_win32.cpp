/*
 * platform_utils_win32.cpp
 *
 *  Created on: 18 Mar 2017
 *      Author: MikoKuta
 */

#include "shared/types.h"
#include "shared/platform_utils.h"
#include <windows.h>

uint64 getCPUFrequency()
{
	LARGE_INTEGER cpuFrequency;
	QueryPerformanceFrequency(&cpuFrequency);
	return (uint64)cpuFrequency.QuadPart;
};

uint64 getTicks()
{
	LARGE_INTEGER cpuCounter;
	QueryPerformanceCounter(&cpuCounter);
	return (uint64)cpuCounter.QuadPart;
}

uint64 getMicroseconds()
{
	LARGE_INTEGER cpuFrequency;
	LARGE_INTEGER cpuCounter;

	QueryPerformanceFrequency(&cpuFrequency);
	QueryPerformanceCounter(&cpuCounter);

	uint64 frameDurationUs = (cpuCounter.QuadPart*1000) / (cpuFrequency.QuadPart / 1000);
	return frameDurationUs;
}

uint64 getMilliseconds()
{
	LARGE_INTEGER cpuFrequency;
	LARGE_INTEGER cpuCounter;

	QueryPerformanceFrequency(&cpuFrequency);
	QueryPerformanceCounter(&cpuCounter);

	uint64 frameDurationMs = (cpuCounter.QuadPart*1000) / cpuFrequency.QuadPart;
	return frameDurationMs;
}
